package ru.t1.sochilenkov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    private String userId;

}
